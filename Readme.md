# Apache Spark integration testing on Scala
Building blocks for integration tests

[ScalaTest](http://www.scalatest.org/) used as test framework, the same as in Spark server [sources](https://github.com/apache/spark).

Documentation available [here](https://spark-testing-java.readthedocs.io/en/latest/)