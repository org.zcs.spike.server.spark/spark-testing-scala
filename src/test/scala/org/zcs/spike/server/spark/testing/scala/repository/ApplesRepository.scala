package org.zcs.spike.server.spark.testing.scala.repository

import org.apache.spark.sql.{Dataset, Encoders, Row}
import org.apache.spark.sql.functions.sum

class ApplesRepository {

  def mass(fruits: Dataset[Row]): Long = fruits.select(sum("weight")).as(Encoders.LONG).first


}
