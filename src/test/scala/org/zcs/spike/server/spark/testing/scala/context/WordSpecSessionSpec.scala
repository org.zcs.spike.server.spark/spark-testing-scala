package org.zcs.spike.server.spark.testing.scala.context

import org.apache.spark.sql.test.SharedSparkSessionBase
import org.scalatest.wordspec.AnyWordSpec
import org.zcs.spike.server.spark.testing.scala.repository.ApplesRepository

class WordSpecSessionSpec extends AnyWordSpec with SharedSparkSessionBase {

  import testImplicits._

  val repository = new ApplesRepository()

  "a Mass" when {
    "when only one apple" should {
      "have to return mass of this apple" in {
        val expected = 80
        val df = spark.createDataset(List(expected)).toDF("weight")
        assert(expected === repository.mass(df))
      }
    }

  }


}
